//Has a left-hand side and a right-hand side
//x is always the variable (for now)
import * as mathjs from 'mathjs';
export type expression = string;

export type equation = {
    LHS: expression[];
    RHS: expression[];
};

export function render_eq (equation: equation) {
    let eq = ["="];
    let all = equation.LHS.concat(eq, equation.RHS);

    return all.join("");
}

function add_to_side (add: expression, expr: expression[]) {
    return expr.concat("+", add)
}

export function add_to_eq(add: expression, eq: equation) {
    eq.LHS = add_to_side(add, eq.LHS);
    eq.RHS = add_to_side(add, eq.RHS);
    return eq
}

function multiply_side (factor: expression, expr: expression[]) {
    expr.unshift("(");
    expr.push(")", "*", factor)
    return expr
}

export function multiply_eq(factor: expression, eq: equation) {
    eq.LHS = multiply_side(factor, eq.LHS);
    eq.RHS = multiply_side(factor, eq.RHS);
    return eq
}

function divide_side (divisor: expression, expr: expression[]) {
    expr.unshift("(");
    expr.push(")", "/", divisor)
    return expr
}

export function divide_eq(divisor: expression, eq: equation) {
    eq.LHS = divide_side(divisor, eq.LHS);
    eq.RHS = divide_side(divisor, eq.RHS);
    return eq
}

export function listToString(expr: expression[]) {
    return expr.join(" ")
}

export function stringToList(string: string): string[] {
    // Break down a sympy expression into a list of strings
    let exprStr: string = string.replace(/\s+/g, ''); // Remove spaces
    const result: string[] = [];
    let currentTerm: string = '';

    for (const char of exprStr) {
        if (!isNaN(Number(char)) || char === '.') {
            currentTerm += char;
        } else if (char.match(/[a-zA-Z]/)) {
            if (currentTerm) {
                result.push(currentTerm);
                currentTerm = '';
            }
            result.push(char);
        } else {
            if (currentTerm) {
                result.push(currentTerm);
                currentTerm = '';
            }
            result.push(char);
        }
    }

    if (currentTerm) {
        result.push(currentTerm);
    }
    return result;
}

export function simplifyString(expr: expression) {
    let res =  mathjs.simplify(expr)
    return mathjs.string(res)
}

export function simplifyExpr(expr: expression[]) {
    let str = listToString(expr)
    let simple = simplifyString(str)
    return stringToList(simple);
}

export function simplify_eq (eq: equation) {
    eq.LHS = simplifyExpr(eq.LHS);
    eq.RHS = simplifyExpr(eq.RHS);
    return eq
}